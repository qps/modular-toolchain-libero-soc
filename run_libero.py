""" Project to hopefully have a more modular toolchain for
    Microsemi SoC v12.x tools.

    Written end of 2019 by Severin Haas, TE-MPE-EP - CERN

    ToDo:
        - Read log files:
            - Timing tables
        - Add all settings to a function call
        - Check file paths again, might not work with linux
"""

import os
import subprocess
import sys
import shutil

# Project settings
LANGUAGE = "vhdl"
SYN_FAMILY = "IGLOO2"
SYN_DEVICE = "M2GL060"
SYN_GRADE = "-1"
SYN_ADV_OPT = []
SYN_PACKAGE = "484 FBGA"
SYN_TOP = "demo"
SYN_PROJECT = "demo"
WORK_MODULE_NAME = SYN_TOP + "::" + "work"

# File location
PATH_SDC_FILE = "./constraints/top.sdc"
PATH_PDC_FILE = "./constraints/top_io.pdc"
PATH_HDL_SRC = ["./src/"]

# Path + filename for buildstamp file. Might also be added to PATH_LIBRARY below.
PATH_GIT_BUILDSTAMP = ""

# Path + filename for radhardlevel file. Might also be added to PATH_LIBRARY below.
PATH_RADHARDLEVEL_FILE = ""

PATH_LIBRARY = {}

PATH_FILENAME = "./path.cfg"
SCRIPT_PATH = "./scripts/"

BUILDSTAMP_BITS = 32


"""
    Probably you do not have to change anything below here :)
"""
if os.name == "nt":
    SYN_TOOL = "libero.exe"
elif os.name == "posix":
    SYN_TOOL = "libero"

# Pre defined TCL "scripts"
TCL_NEW_PROJECT = 'new_project -location {{{0}}} ' + \
                  '-name {{{1}}} -hdl {{{2}}} ' + \
                  '-family {{{3}}} -die {{{4}}} ' + \
                  '-package {{{5}}} -speed {{{6}}}'
TCL_ADV_OPTION = ' -adv_options {{{0}}}'
TCL_TOOLS_SYNTH = 'organize_tool_files -tool {{SYNTHESIZE}} ' + \
                  '-file {{{0}}} -module {{{1}}} ' + \
                  '-input_type {{constraint}}\n'
TCL_TOOLS_PLACEROUTE = 'organize_tool_files -tool {{PLACEROUTE}} ' + \
                       '-file {{{0}}} -file {{{1}}} ' + \
                       '-module {{{2}}} ' + \
                       '-input_type {{constraint}}\n'
TCL_ROOT_MODULE = 'set_root -module {{{0}}}\n'
TCL_OPEN_PROJECT = 'open_project -file {{{0}}}\n'
TCL_SAVE_PROJECT = 'save_project\n'
TCL_CLOSE_PROJECT = 'close_project\n'
TCL_LOAD_FILES = 'source {0}\n' + \
                 'refresh\n'
TCL_SYNTH = 'run_tool -name {SYNTHESIZE}\n'
TCL_BITSTREAM = 'run_tool -name {GENERATEPROGRAMMINGDATA}\n'
TCL_EXPORT_BITSTREAM = 'export_bitstream_file ' + \
                       '-file_name {{{0}}} ' + \
                       '-export_dir {{{1}}} ' + \
                       '-format {{STP}} ' + \
                       '-trusted_facility_file 1 ' + \
                       '-trusted_facility_file_components {{FABRIC}} ' + \
                       '-serialization_stapl_type {{SINGLE}} ' + \
                       '-serialization_target_solution {{FLASHPRO_3_4_5}}\n'
TCL_HDL_FILE = 'create_links -hdl_source {0}\n'
TCL_SDC_FILE = 'create_links -sdc {0}\n'
TCL_PDC_FILE = 'create_links -io_pdc {0}\n'
TCL_ADD_LIBRARY = 'add_library -library {0}\n'
TCL_ADD_LIBRARY_FILE = 'add_file_to_library -library {0} -file {1}\n'

SYN_RADHRADLEVEL_FILE = 'library ieee;\n' + \
                        'use ieee.std_logic_1164.all;\n' + \
                        'use ieee.numeric_std.all;\n\n' + \
                        'package rad_control_pkg is\n' + \
                        '\tconstant rad_control: string := "{0}";\n' + \
                        'end rad_control_pkg;\n\n' + \
                        'package body rad_control_pkg is \nend rad_control_pkg;'
HELP_RADHARDLEVEL = 'Please enter a value of the triplication level. ' + \
                    'Values can be:\n' + \
                    '  none   - Use standard design techniques\n' + \
                    '  cc     - Use C-C implementation\n' + \
                    '  tmr    - Use TMR implementation\n' + \
                    '  tmr_cc - Use TMR_CC implementation\n' + \
                    'For details see Application Note AC139 from Microsemi.\n'
INCLUDE_RADHARDLEVEL = 'Please note, that the {0} package has also to be added ' + \
                       'to the top level file.\n' + \
                       'For example:\n' + \
                       'use LIB_NAME.rad_control_pkg.all;\n\n' + \
                       'and at the beginning of the architecture:\n' + \
                       'attribute syn_radhardlevel : string;\n' + \
                       'attribute syn_radhardlevel of TOP_ARCH_NAME: ' + \
                       'architecture is rad_control;\n'

GIT_HASH_CMD = 'git rev-parse HEAD'
BUILDSTAMP_FILE = 'library ieee;\n' + \
                  'use ieee.std_logic_1164.all;\n' + \
                  'use ieee.numeric_std.all;\n\n' + \
                  'package buildstamp is\n' + \
                  '\tconstant build_rev : std_logic_vector({0} downto {1}) := x"{2}";\n' + \
                  'end buildstamp;\n\n' + \
                  'package body buildstamp is \n' + \
                  'end buildstamp;\n'

# Some Microsemo Libero specific stuff
PROJECT_FILE_ENDING = ".prjx"
LIBERO_SCRIPT_CMD = "SCRIPT:{0}"
LIBERO_LOG_CMD = "LOGFILE:{0}"


def print_help():
    """ Function to print help/error screen to the user.
    """
    help_menu = 'General help menu for ' + str(SYN_PROJECT) + ' project.\n' + \
                'Usage: python run_libero.py CMD,' + \
                'e.g.: python run_libero project\n' + \
                '\nAvailable commands:\n' + \
                '  help      shows this menu\n' + \
                '  clean     deletes all folder and files which were created by this script\n' + \
                '  git       creates a file which contains the git hash which can be ' + \
                'used as a buildstamp\n' + \
                '  rad VALUE seletcs which grade of triplication shall be used. ' + \
                'Standard selection is none\n' + \
                '    Avaliable commands for VALUE are:\n' + \
                '      none   - Use standard design techniques\n' + \
                '      cc     - Use C-C implementation\n' + \
                '      tmr    - Use TMR implementation\n' + \
                '      tmr_cc - Use TMR_CC implementation\n' + \
                '    For details see Application Note AC139 from Microsemi.\n' + \
                '\nCommands for synthesis and bitstream generation:\n' + \
                '  project  creates a new Libero SoC project\n' + \
                '  synth    synthesis \n' + \
                '  impl     place and route and bitstream file export\n' + \
                '\nCommands to show log files:\n' + \
                '  log -project         Show project log file\n' + \
                '  log -synth           Show synthesis log file\n' + \
                '  log -synth_version   Show version of the used synthesis tool\n' + \
                '  log -synth_config    Show synthesis configuration\n' + \
                '  log -synth_timing    Show resource usage\n' + \
                '  log -impl            Show implementation log\n' + \
                '  log -impl_timing     Show resource usage\n' + \
                '  log -resources       Show resource usage\n' + \
                '  log -dev_settings    Show device settings\n'

    print(help_menu)


def delete_newline_sign(input_string):
    """ Function to delete a newline sign at the end of a string.
        Necessary for example when files are read.
    """
    new_string = input_string[0:input_string.find("\n")]
    return new_string


def read_path_file(filename):
    """ Function to read a path.cfg file.
        The file shall contain the following settings:
            - libero_path=PATH_TO_LIBERO
            - libero_project=PATH_TO_PROJECT
            - radhard_path=PATH_TO_CONFIG_FILE
    """
    libero_path = ""
    project_path = ""

    if os.path.isfile(filename):
        with open(filename, 'r') as path_file:
            lines = path_file.readlines()

        for path_line in lines:
            split_line = path_line.split('=')

            # Find which is the right line with the setting path or project
            if split_line[0].find("libero_path") == 0:
                libero_path = delete_newline_sign(split_line[1])
            elif split_line[0].find("libero_project") == 0:
                project_path = delete_newline_sign(split_line[1])
    else:
        print("File does not exist. Please add a path.cfg file which " + \
              "contains the path to libero and the project location.\n" + \
              "If necessary it can also contain a path where the " + \
              "radhardlevel file shall be put.\n"
              "See README for details.")
        return -1

    return [libero_path, project_path]


def list_files(dir_path, file_type):
    """ Function to list all files in a given directory
    with a given file ending.
    """
    file_list = os.listdir(dir_path)
    hdl_files = []

    for new_filename in file_list:
        if file_type in new_filename:
            hdl_files.append(new_filename)

    return hdl_files


def folder_available(folder_path):
    """ Function to check if a folder is already there.
    """
    if os.path.exists(folder_path):
        return 1

    return 0


def write_project_file(project_path, work_module):
    """ Function to write a 'project.tcl' file which shall be used
        to create a new project via Libero SoC and configure it.
    """
    # Check if folder is available, otherwise create it
    if folder_available(SCRIPT_PATH) == 0:
        create_folder(SCRIPT_PATH)

    # Open/Create file
    project_file = open(SCRIPT_PATH + 'project.tcl', 'w+')

    # Write settings for new project
    settings = TCL_NEW_PROJECT.format(project_path, SYN_PROJECT, LANGUAGE,
                                      SYN_FAMILY, SYN_DEVICE, SYN_PACKAGE,
                                      SYN_GRADE, SYN_ADV_OPT)

    # Check if there are advanced options and if yes, add them all
    if len(TCL_ADV_OPTION) > 0:
        for adv_option in SYN_ADV_OPT:
            settings = settings + TCL_ADV_OPTION.format(adv_option)

    project_file.write(settings + "\n")

    # Open project
    project_file.write(TCL_OPEN_PROJECT.format(project_path + "\\" +
                                               SYN_PROJECT +
                                               PROJECT_FILE_ENDING))

    # Add files to project
    project_file.write(TCL_LOAD_FILES.format(SCRIPT_PATH + "files.tcl"))

    # Write tool specific files (sdc & pdc)
    project_file.write(TCL_TOOLS_SYNTH.format(PATH_SDC_FILE, work_module))
    project_file.write(TCL_TOOLS_PLACEROUTE.format(PATH_SDC_FILE,
                                                   PATH_PDC_FILE, work_module))

    # Write root module
    project_file.write(TCL_ROOT_MODULE.format(work_module))

    # Save and close project
    project_file.write(TCL_SAVE_PROJECT)
    project_file.write(TCL_CLOSE_PROJECT)

    project_file.close()


def write_file(path_src_files, path_sdc, path_pdc, hdl_language):
    """ Function to write a 'file.tcl' file which shall be used
        to add all necessary files to the Libero SoC project.
    """
    # Check if folder is available, otherwise create it
    if folder_available(SCRIPT_PATH) == 0:
        create_folder(SCRIPT_PATH)

    # Open/Create file
    src_file = open(SCRIPT_PATH + 'files.tcl', 'w+')

    # check only for .vhd or .v files
    if hdl_language == "vhdl":
        file_ending = ".vhd"
    elif hdl_language == "verilog":
        file_ending = ".v"

    for new_path in path_src_files:
        hdl_files = list_files(new_path, file_ending)

        # HDL files in one directory
        for new_file in hdl_files:
            filename = new_path + new_file
            # Write filename and command to file
            src_file.write(TCL_HDL_FILE.format(filename))

    # Add library to project if needed
    if len(PATH_LIBRARY) != 0:
        # Extract all lib names
        lib_names = list(PATH_LIBRARY)

        for new_lib in lib_names:
            # Add lib
            src_file.write(TCL_ADD_LIBRARY.format(new_lib))

            # Add files to the new created lib
            lib_paths = PATH_LIBRARY[new_lib]
            for new_lib_path in lib_paths:
                src_file.write(TCL_ADD_LIBRARY_FILE.format(new_lib, new_lib_path))

    # Write SDC constraint file
    src_file.write(TCL_SDC_FILE.format(path_sdc))
    # Write PDC constraint file
    src_file.write(TCL_PDC_FILE.format(path_pdc))

    src_file.close()


def write_synth_file(project_path):
    """ Function to write a 'synth.tcl' file which shall be used
        to synthesize a given project via Libero SoC.
    """
    # Check if folder is available, otherwise create it
    if folder_available(SCRIPT_PATH) == 0:
        create_folder(SCRIPT_PATH)

    # Open/Create file
    synth_file = open(SCRIPT_PATH + 'synth.tcl', 'w+')

    # Open project
    full_project_path = project_path + "\\" + SYN_PROJECT + PROJECT_FILE_ENDING
    synth_file.write(TCL_OPEN_PROJECT.format(full_project_path))

    # Write commands for synthesis
    synth_file.write(TCL_SYNTH)

    # Save and close project
    synth_file.write(TCL_SAVE_PROJECT)
    synth_file.write(TCL_CLOSE_PROJECT)

    synth_file.close()


def write_bitstream_file(project_path):
    """ Function to write a 'bitstream.tcl' file which shall be used
        to build a bitstream via Libero SoC.
    """
    # Check if folder is available, otherwise create it
    bitstream_folder = project_path + "\\bitstream"
    if folder_available(bitstream_folder) == 0:
        create_folder(bitstream_folder)

    # Open/Create file
    synth_file = open(SCRIPT_PATH + 'bitstream.tcl', 'w+')

    # Open project
    full_project_path = project_path + "\\" + SYN_PROJECT + PROJECT_FILE_ENDING
    synth_file.write(TCL_OPEN_PROJECT.format(full_project_path))

    # Write commands for bitstream generation
    synth_file.write(TCL_BITSTREAM)

    # Write bitstream export settings
    synth_file.write(TCL_EXPORT_BITSTREAM.format(SYN_PROJECT, bitstream_folder))

    # Save and close project
    synth_file.write(TCL_SAVE_PROJECT)
    synth_file.write(TCL_CLOSE_PROJECT)

    synth_file.close()


def write_radhardlevel(filename, radhardlevel_value):
    """ Function to write a file with the settings for microsemis syn_radhardlevel
    attributue to perform Triple modular redundancy (TMR).
    """
    tmr_file = open(filename, 'w+')
    tmr_file.write(SYN_RADHRADLEVEL_FILE.format(radhardlevel_value))
    tmr_file.close()


def write_git_revision(filename, git_hash, num_bits):
    """ Function to write a file with the settings for microsemis syn_radhardlevel
    attributue to perform Triple modular redundancy (TMR).
    """
    git_rev_file = open(filename, 'w+')
    git_rev_file.write(BUILDSTAMP_FILE.format(str(num_bits - 1), str(0), git_hash))
    git_rev_file.close()


def execute_libero_script(libero_path, tool, tcl_filename, log_filename):
    """ Function to execute TCL scripts via Microsemi Libero software.
    """
    full_tool_path = libero_path + tool

    if os.path.isfile(full_tool_path):
        libero_os_cmd = full_tool_path + " " + \
                        LIBERO_SCRIPT_CMD.format(tcl_filename) + " " + \
                        LIBERO_LOG_CMD.format(log_filename)
        print(libero_os_cmd)
        os.system(libero_os_cmd)
    else:
        print("File " + full_tool_path + " not available!")


def clean_folder(clean_directory):
    """ Function to clean/remove the folder which is mentioned
    via clean_directory
    """
    if os.path.exists(clean_directory):
        shutil.rmtree(clean_directory)
        print("The directory " + clean_directory + " was removed")
    return 0


def remove_file(remove_file_path):
    """ Function to remove a file.
    """
    if os.path.isfile(remove_file_path):
        os.remove(remove_file_path)
        print("The file " + remove_file_path + " was removed")
    return 0


def create_folder(folder_path):
    """ Function to create a folder.
    """
    try:
        os.mkdir(folder_path)
    except OSError:
        print("Could not create the directory " + folder_path)


def cat_file(cat_file_path):
    """ Function to basically implement the 'cat' commandline tool.
        It seems os.system() does not support cat but log-files shall
        be printed. So this function can be used for it.
    """
    file_content = ""

    if os.path.isfile(cat_file_path):
        with open(cat_file_path) as file_handler:
            file_content += file_handler.read()
    else:
        print("File " + cat_file_path + " not available!")

    return file_content


def parse_resources(parse_file_path):
    """ Function to parse a implementation log file. It parses the content
    of a given file and extracts the resources table.
    """
    resource_table = ""
    table_found = False
    counter = 0

    if os.path.isfile(parse_file_path):
        bitstream_file = open(parse_file_path, 'r')

        for line in bitstream_file:
            # The table starts with the string "Resource Usage"
            if line.find("Resource Usage") == 0:
                table_found = True

            # Extract the table. Expected length is 16 characters
            if table_found and counter < 17:
                counter = counter + 1
                resource_table = resource_table + line
    else:
        print("File " + parse_file_path + " not available!")

    return resource_table


def step_success(step_file_path):
    """ Function which checks if a step was finished succesfully.
    It searchs in the given log file the string:
        "The Execute Script command succeeded."
    """
    search_string = "The Execute Script command succeeded"
    success = False

    if os.path.isfile(step_file_path):
        log_file = open(step_file_path, 'r')

        for line in log_file:
            if line.find(search_string) == 0:
                success = True
    else:
        print("File " + step_file_path + " not available!")

    return success


def get_git_hash(num_hex_digits):
    """ Function which returns the last digits of a git hash which can then
    be put into e.g. the register map.
    """
    # Check git revision hash via cmdline
    git_rev = subprocess.run(GIT_HASH_CMD, encoding='utf-8', \
                             stdout=subprocess.PIPE, check=True)
    git_full_hash = delete_newline_sign(git_rev.stdout)

    # Resize the hash to the number of digits needed
    if len(git_full_hash) > 0:
        resized_hash = git_full_hash[len(git_full_hash) - num_hex_digits:len(git_full_hash)]
    return resized_hash



if __name__ == '__main__':
    #
    # Check which command was entered by the user
    #
    if len(sys.argv) == 1:
        print_help()

    if len(sys.argv) == 2:
        if sys.argv[1] == "clean":
            clean_folder("./implementation")
            clean_folder("./scripts")
            remove_file("project.log")
            remove_file("synth.log")
            remove_file("bitstream.log")

        elif sys.argv[1] == "help":
            print_help()

        elif sys.argv[1] == "project":
            # Read path file
            [LIBERO_PATH, PROJECT_PATH] = read_path_file(PATH_FILENAME)

            # Write project.tcl file
            write_project_file(PROJECT_PATH, WORK_MODULE_NAME)
            print("project.tcl file written")

            # Write files.tcl file
            write_file(PATH_HDL_SRC, PATH_SDC_FILE, PATH_PDC_FILE, LANGUAGE)
            print("files.tcl file written")

            # Check if projct is already there
            if folder_available(PROJECT_PATH):
                print("Project already exists, will be removed")
                clean_folder(PROJECT_PATH)

            # Execute the script via libero
            execute_libero_script(LIBERO_PATH, SYN_TOOL,
                                  SCRIPT_PATH + "project.tcl", "project.log")
            # Read the log file
            print("\nLog-File entry:")
            print(cat_file("project.log"))

            if step_success("project.log"):
                print("Project generation finished successfully.")
                print("Created Libero SoC project: " + SYN_PROJECT + "/" +
                      SYN_PROJECT + PROJECT_FILE_ENDING)
            else:
                print("Project generation finished with errors. " + \
                      "Check log file.")

        elif sys.argv[1] == "synth":
            # Read path file
            [LIBERO_PATH, PROJECT_PATH] = read_path_file(PATH_FILENAME)

            # Write synth.tcl file
            write_synth_file(PROJECT_PATH)
            print("synth.tcl file written")

            # Execute the script via libero
            execute_libero_script(LIBERO_PATH, SYN_TOOL,
                                  SCRIPT_PATH + "synth.tcl", "synth.log")
            # Read the log file
            print(cat_file("synth.log"))

            if step_success("synth.log"):
                print("Synthesize finished successfully.")
            else:
                print("Synthesize finished with errors. " + \
                      "Check log file.")

        elif sys.argv[1] == "impl":
            # Read path file
            [LIBERO_PATH, PROJECT_PATH] = read_path_file(PATH_FILENAME)

            # Write bitstream.tcl file
            write_bitstream_file(PROJECT_PATH)
            print("bitstream.tcl file written")

            # Execute the script via libero
            execute_libero_script(LIBERO_PATH, SYN_TOOL,
                                  SCRIPT_PATH + "bitstream.tcl",
                                  "bitstream.log")
            # Read the log file
            print(cat_file("bitstream.log"))

            # Show the files which are written to the bitstream folder
            BITSTREAM_PATH = PROJECT_PATH + "/bitstream"

            if step_success("bitstream.log"):
                print("Bitstream generation finished successfully.")
            else:
                print("Bitstream generation finished with errors. " + \
                      "Check log file.")

            # Only show directory content when it is available
            if os.path.exists(BITSTREAM_PATH):
                bitstream_directory = os.listdir(BITSTREAM_PATH)
                print("Generated bitstream files: " + str(bitstream_directory))
            else:
                print("No Bitstream found. Check log-file.")

        elif sys.argv[1] == "git":
            # Each hexadceimal digit = 4 Bits, therefore convert bits to hex digits
            num_digits = int(BUILDSTAMP_BITS / 4)

            # Get the last 8 digits of the git hash
            git_repo_hash = get_git_hash(num_digits)
            print("Git hash: " + git_repo_hash)

            if PATH_GIT_BUILDSTAMP == "":
                print("Please enter a valid path to the buildstamp file.")
            else:
                write_git_revision(PATH_GIT_BUILDSTAMP, git_repo_hash, BUILDSTAMP_BITS)

        else:
            print_help()

    if len(sys.argv) == 3:
        if sys.argv[1] == "rad":
            if sys.argv[2].lower() == "none" or \
               sys.argv[2].lower() == "cc" or \
               sys.argv[2].lower() == "tmr" or \
               sys.argv[2].lower() == "tmr_cc":
                [LIBERO_PATH, PROJECT_PATH] = read_path_file(PATH_FILENAME)

                if PATH_RADHARDLEVEL_FILE == "":
                    print("No file path for the radhard package file given. " + \
                          "Please add a path and filename to the file (see README).")
                else:
                    write_radhardlevel(PATH_RADHARDLEVEL_FILE, sys.argv[2].lower())
                    print("Written settings to " + PATH_RADHARDLEVEL_FILE + " file.\n")
                    print(INCLUDE_RADHARDLEVEL.format(PATH_RADHARDLEVEL_FILE))
            else:
                print(HELP_RADHARDLEVEL)

        elif sys.argv[1] == "log":
            if sys.argv[2] == "-project":
                print(cat_file("project.log"))

            elif sys.argv[2] == "-synth":
                print(cat_file("synth.log"))

            elif sys.argv[2] == "-synth_version":
                [LIBERO_PATH, PROJECT_PATH] = read_path_file(PATH_FILENAME)
                print(cat_file(PROJECT_PATH + "\\synthesis\\version.log"))

            elif sys.argv[2] == "-synth_config":
                [LIBERO_PATH, PROJECT_PATH] = read_path_file(PATH_FILENAME)
                print(cat_file(PROJECT_PATH + "\\synthesis\\run_options.txt"))

            elif sys.argv[2] == "-impl":
                print(cat_file("bitstream.log"))

            elif sys.argv[2] == "-resources":
                resources = parse_resources("bitstream.log")
                print(resources)

            elif sys.argv[2] == "-dev_settings":
                [LIBERO_PATH, PROJECT_PATH] = read_path_file(PATH_FILENAME)
                print(cat_file(PROJECT_PATH + "\\tooldata\\" +
                               SYN_PROJECT + ".log"))

            elif sys.argv[2] == "-synth_timing":
                # TBD: find file to parse
                print("Coming soon!")

            elif sys.argv[2] == "-impl_timing":
                # TBD: find file to parse
                print("Coming soon!")
            else:
                print_help()
        else:
            print_help()

    if len(sys.argv) > 3:
        print_help()
