# Modular Toolchain for Microsemi Libero SoC
This repository contains the script for the modular firmware toolchain for Microsemi Libero SoC.
It consists of one script (run_libero.py) which has to be configured (see below) for each project and that’s it. 
Every time the script is executed it writes the TCL files for Libero and executes them via Libero SoC. 

**Note:** this is work in progress and will not work out of the box as there are no example HDL files added.


## Tools to be used
The following tools were used for building and testing:
  * Microsemi Libero SoC v12.2 with a valid license for Libero and Synplify
  * Python 3.x, tested with Python 3.7.x
  * A command line like PowerShell
  * A text editor of your choice
  * Coffee or tee


## How to build a FPGA image
  * Open the file run_libero.py in a text editor
  * If necessary change the settings on top of the script
    * Normally you just have to change the upper few lines.
    * If - for example - you do not want to set adv_options then do not delete the line, but leave the array empty:
    ```python
    SYN_ADV_OPT = []
    ```
    The same for e.g. libraries:
    ```python
    PATH_LIBRARY = {}
    ```
    Note, that here a python specific data type is used: a dictionary.
    When you want to add a library, you have to enter the values in the given dictionary, e.g.:
    ```python
    PATH_LIBRARY = {"your_lib_name": ["./src/lib/your_lib_file1.vhd",
                                      "./src/lib/your_lib_file2.vhd"]}
    ```
  * Add a file called "path.cfg" to the main folder
    * The file shall contain the item libero_path and libero_project. The following can be used as an example:
    ```
    libero_path=C:\Microsemi\Libero_SoC_v12.2\Designer\bin\
    libero_project=C:\hdl_code\hdsm_hsu_fw\implementation
    ```
  * Open a command line of your choice (e.g. PowerShell) and type 
  ```powershell
  python run_libero.py project 
  ```
  to create a Libero project.
  * When it was executed correctly you can start the synthesize via
  ```powershell
  python run_libero.py synth 
  ```
  * Finally: to start the bitstream generation type
  ```powershell
  python run_libero.py impl 
  ```
  The step "synth" might be skipped, because Libero will take care of synth by itself if it was not yet executed.
  * Sit back and have a coffee


## Other available commands
  * All other commands which are available can be seen via
    ```powershell
    python run_libero.py help
    ```
